// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDEWSRYuYivITHevduscyVWljUFEfPC7-Y",
    authDomain: "leads-generator-c2553.firebaseapp.com",
    projectId: "leads-generator-c2553",
    storageBucket: "leads-generator-c2553.appspot.com",
    messagingSenderId: "296185657704",
    appId: "1:296185657704:web:29aede4cf747691850fedd",
    measurementId: "G-6EJ8P6L5GP"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
