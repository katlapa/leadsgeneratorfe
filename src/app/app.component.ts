import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { NavigationEnd, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { DeviceDetectorService } from 'ngx-device-detector';
import Swal from 'sweetalert2'
import { LanguageService } from './language.service';
import { Subscription } from 'rxjs';
import { ViewportScroller } from '@angular/common';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  // device detection
  isMobile;
  isTablet;
  isDesktop;

  routerSubs: Subscription;
  langSub: Subscription;

  config: MatDialogConfig = {
    disableClose: false,
    hasBackdrop: true,
    backdropClass: '',
    width: '360px',
    height: '',
    panelClass:'makeItMiddle', //Class Name that can be defined in styles.css as follows:
};

  // language
  languages = [
    { value: 'en', label: 'English', img: 'assets/svg/gb.svg' }, 
    { value: 'it', label: 'Italiano', img: 'assets/svg/it.svg'}, 
    { value: 'es', label: 'Español', img: 'assets/svg/es.svg' },
    { value: 'gr', label: 'Ελληνικά', img: 'assets/svg/gr.svg' },
    { value: 'de', label: 'Deutsch', img: 'assets/svg/de.svg' }
  ];
  language = this.languages[0];

  constructor(
    private _db: AngularFirestore, 
    private _translate: TranslateService,
    private _deviceDetect: DeviceDetectorService,
    private _dialog: MatDialog,
    private _router: Router,
    private _lang: LanguageService,
    private _viewportScroller: ViewportScroller) {
    this._translate.setDefaultLang('en')
      this.isMobile = this._deviceDetect.isMobile();
      this.isTablet = this._deviceDetect.isTablet();
      this.isDesktop = this._deviceDetect.isDesktop();
      this.routerSubs = this._router.events
      // listen for route onInit
      .subscribe((res) => {
        if(res instanceof NavigationEnd) {
          if(res.url !== '/') {
            switch (res.url.slice(1, 3)) {
              case "en":
                this.language = { value: 'en', label: 'English', img: 'assets/svg/gb.svg' }
                this._lang.sendActiveLang("en")
                break; 
              case "it": 
                this.language = { value: 'it', label: 'Italiano', img: 'assets/svg/it.svg'}
                this._lang.sendActiveLang("it")
                break;
              case "es":
                this.language = { value: 'es', label: 'Español', img: 'assets/svg/es.svg' }
                this._lang.sendActiveLang("es")
                break;
              case "gr": 
                this.language = { value: 'gr', label: 'Ελληνικά', img: 'assets/svg/gr.svg' }
                this._lang.sendActiveLang("gr")
                break;
              case "de": 
                this.language = { value: 'de', label: 'Deutsch', img: 'assets/svg/de.svg' }
                this._lang.sendActiveLang("gr")
            }
            this._translate.use(res.url.slice(1, 3))
          } else {
            this._translate.use('en')
          }
        }
      })
      // check for language change
      this.langSub = this._translate.onLangChange.subscribe(res => {
        this._lang.sendActiveLang(res.lang)
      })
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.routerSubs.unsubscribe();
    this.langSub.unsubscribe();
  }

  openDialog() {
    this._dialog.open(RegisterDialog, this.config);
  }

  selectLanguage(value: string) {
    this.languages.map((lang) => {
      if(lang.value === value) {
        this.language = lang;
        this._translate.use(value)
      }
    })
  }

  goToInsideLink(link: string, drawer?) {
    if(drawer) {
      drawer.close()
      this._viewportScroller.scrollToAnchor(link)
      document.getElementById(link).scrollIntoView({ 
        behavior: 'smooth' 
      });
    } else {
      this._viewportScroller.scrollToAnchor(link)
    }
  } 
  open() {
    this.openDialog()
  }
}


@Component({
  selector: 'register-dialog',
  templateUrl: './register-dialog.html',
})
export class RegisterDialog {
  telHasError = false;
  loadingSpinner = false;
  activeLanguage: any;
  langSubs: Subscription;
  // register user form
  userForm = new FormGroup({
    emer: new FormControl('', Validators.required),
    mbiemer: new FormControl('', Validators.required),
    email: new FormControl('', Validators.email),
    telefon: new FormControl('', Validators.required)
  })

  constructor(
    private _db: AngularFirestore,
    private _lang: LanguageService,
    // private _dialog: MatDialog
    ) {
      this.langSubs = this._lang.activeLang.subscribe(res => {
        this.activeLanguage = res;
      })
    }

    ngOnDestroy(){
      this.langSubs.unsubscribe();
    }

  createUser() {
    let userInfo = {
      ...this.userForm.value,
      date: new Date()
    }
    switch(this.activeLanguage) {
      case "en":
        this._db.collection('English').add(userInfo).then(res => {
          Swal.fire({
            title: "You can now be your own boss!",
            text: "One of our experts will contact you shortly.",
            icon: "success",
            confirmButtonText: "Okay!"
          })
        })
        break;
      case "it":
        this._db.collection('Italian').add(userInfo).then( res => {
          Swal.fire({
            title: "Ora puoi essere il capo di te stesso!",
            text: "Uno dei nostri esperti ti contatterà a breve.",
            icon: "success",
            confirmButtonText: "Va Bene!"
          })
        })
        break;
      case "es": 
      this._db.collection('Spanish').add(userInfo).then(res => {
        Swal.fire({
          title: "Ahora puedes ser tu propio jefe!",
          text: "Uno de nuestros expertos se pondrá en contacto contigo a la brevedad.",
          icon: "success",
          confirmButtonText: "Bueno!"
        })
      })
      break;
      case "gr": 
      this._db.collection('Greek').add(userInfo).then(res => {
        Swal.fire({
          title: "Τώρα μπορείς να είσαι το αφεντικό του εαυτού σου!",
          text: "Ένας από τους ειδικούς μας θα επικοινωνήσει μαζί σας σύντομα.",
          icon: "success",
          confirmButtonText: "εντάξει!"
        })
      })
      break;
      case "de": 
      this._db.collection('German').add(userInfo).then(res => {
        Swal.fire({
          title: "Du kannst jetzt dein eigener Chef sein!",
          text: "Einer unserer Experten wird sich in Kürze mit Ihnen in Verbindung setzen.",
          icon: "success",
          confirmButtonText: "Gut!"
        })
      })
      // this._dialog.closeAll()
    }
  }

  hasError($event: any) {
    if($event === false) {
      this.telHasError = true
    } else {
      this.telHasError = false;
    }
  }

 

  getNumber($event: any) {
    console.log($event)
  }
}