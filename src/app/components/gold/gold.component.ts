import { Component, OnInit } from '@angular/core';
import { DeviceDetectorService } from 'ngx-device-detector';

@Component({
  selector: 'app-gold',
  templateUrl: './gold.component.html',
  styleUrls: ['./gold.component.scss']
})
export class GoldComponent implements OnInit {
  multi: any[];
  isMobile;
  value = 200;
  winValue: number = 35.50;
  // options
  legend: boolean = true;
  showLabels: boolean = true;
  animations: boolean = true;
  xAxis: boolean = true;
  yAxis: boolean = true;
  showYAxisLabel: boolean = true;
  showXAxisLabel: boolean = true;
  xAxisLabel: string = 'Last Month';
  yAxisLabel: string = 'Price';
  timeline: boolean = true;

  colorScheme = {
    domain: ['#e08e14']
  };

  constructor(private  _deviceDetect: DeviceDetectorService) {
    this.isMobile = this._deviceDetect.isMobile();
    this.multi = multi;
  }

  onSelect(data: any): void {
    console.log('Item clicked', JSON.parse(JSON.stringify(data)));
  }

  onActivate(data: any): void {
    console.log('Activate', JSON.parse(JSON.stringify(data)));
  }

  onDeactivate(data: any): void {
    console.log('Deactivate', JSON.parse(JSON.stringify(data)));
  }

  calculate(val: number) {
    this.winValue = (val/5.634);
  }

  ngOnInit(): void {}
}


export var multi = [
  {
    "name": "Tesla",
    "series": [
      {
        "name": "30 Jul",
        "value": 687
      },
      {
        "name": "2 Aug",
        "value": 709
      },
      {
        "name": "3 Aug",
        "value": 709
      },
      {
        "name": "5 Aug",
        "value": 714
      },
      {
        "name": "6 Aug",
        "value": 699
      },
      {
        "name": "9 Aug",
        "value": 713
      },
      {
        "name": "11 Aug",
        "value": 707
      },
      {
        "name": "13 Aug",
        "value": 717
      },
      {
        "name": "17 Aug",
        "value": 665
      },
      {
        "name": "18 Aug",
        "value": 688
      },
      {
        "name": "23 Aug",
        "value": 706
      },
      {
        "name": "26 Aug",
        "value": 701
      }
    ]
  }
];
