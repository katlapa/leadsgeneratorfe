import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DeviceDetectorService } from 'ngx-device-detector';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-tesla',
  templateUrl: './tesla.component.html',
  styleUrls: ['./tesla.component.scss']
})
export class TeslaComponent implements OnInit {
  multi: any[];
  isMobile;
  value = 200;
  winValue: number = 35.50;
  @Output() open: EventEmitter<any> = new EventEmitter();
  // options
  legend: boolean = true;
  showLabels: boolean = true;
  animations: boolean = true;
  xAxis: boolean = true;
  yAxis: boolean = true;
  showYAxisLabel: boolean = true;
  showXAxisLabel: boolean = true;
  xAxisLabel: string = 'Last Month';
  yAxisLabel: string = 'Price';
  timeline: boolean = true;

  colorScheme = {
    domain: ['#a52a2a']
  };

  constructor(private  _deviceDetect: DeviceDetectorService, private lang: TranslateService) {
    this.isMobile = this._deviceDetect.isMobile();
    this.multi = multi;
  }

  onSelect(data: any): void {
    console.log('Item clicked', JSON.parse(JSON.stringify(data)));
  }

  onActivate(data: any): void {
    console.log('Activate', JSON.parse(JSON.stringify(data)));
  }

  onDeactivate(data: any): void {
    console.log('Deactivate', JSON.parse(JSON.stringify(data)));
  }

  calculate(val: number) {
    this.winValue = (val/5.634);
  }

  openRegister() {
    this.open.emit()
  }

  ngOnInit(): void {}

}




export var multi = [
  {
    "name": "Tesla",
    "series": [
      {
        "name": "30 Jul",
        "value": 687
      },
      {
        "name": "2  Αύγ",
        "value": 709
      },
      {
        "name": "3  Αύγ",
        "value": 709
      },
      {
        "name": "5  Αύγ",
        "value": 714
      },
      {
        "name": "6  Αύγ",
        "value": 699
      },
      {
        "name": "9  Αύγ",
        "value": 713
      },
      {
        "name": "11  Αύγ",
        "value": 707
      },
      {
        "name": "13  Αύγ",
        "value": 717
      },
      {
        "name": "17  Αύγ",
        "value": 665
      },
      {
        "name": "18  Αύγ",
        "value": 688
      },
      {
        "name": "23  Αύγ",
        "value": 706
      },
      {
        "name": "26  Αύγ",
        "value": 701
      }
    ]
  }
];
