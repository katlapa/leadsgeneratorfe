import { Injectable } from "@angular/core";  
import { BehaviorSubject } from "rxjs";
  
@Injectable()  
export class LanguageService {  
    private langSubject: BehaviorSubject < any > = new BehaviorSubject < any > ('en');
    activeLang = this.langSubject.asObservable();

    constructor() {}  

    sendActiveLang(lang: string) {
        this.langSubject.next(lang)
    }

}